package com.bbva.tienda.controller;

import com.bbva.tienda.model.ProductoModel;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/v2")
public class ProductosV2Controller {

    private ArrayList<ProductoModel> listaProductos = null;

    public ProductosV2Controller() {
        listaProductos = new ArrayList<>();
        listaProductos.add(new ProductoModel(1, "PR1",27.35, "blanco"));
        listaProductos.add(new ProductoModel(2, "PR2", 18.33, "negro"));
    }

    /* Get lista de productos */
    @GetMapping(value = "productos", produces = "application/json")
    public ResponseEntity<List<ProductoModel>> obtenerListado()
    {
        System.out.println("Estoy en obtener");
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping("/productos/{id}")
    public ResponseEntity<ProductoModel> obtenerProductoPorId(@PathVariable int id)
    {
        ProductoModel resultado = null;
        ResponseEntity<ProductoModel> respuesta = null;
        try
        {
            resultado = listaProductos.get(id);
            respuesta = new ResponseEntity<>(resultado, HttpStatus.OK);
        }
        catch (Exception ex)
        {
            //String mensaje = "No se ha encontrado el producto";
            //respuesta = new ResponseEntity(mensaje, HttpStatus.NOT_FOUND);
            respuesta = new ResponseEntity(resultado, HttpStatus.NOT_FOUND);
        }
        return respuesta;
    }

    /* Add nuevo producto */
    @PostMapping(value = "/productos", produces="application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoNuevo) {
        System.out.println("Estoy en añadir");
        System.out.println(productoNuevo.getId());
        System.out.println(productoNuevo.getNombre());
        System.out.println(productoNuevo.getPrecio());
        //listaProductos.add(new Producto(99, nombre, 100.5));
        listaProductos.add(productoNuevo);
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    /* Add nuevo producto con nombre */
    @PostMapping(value = "/v2/productos/{nom}/{cat}", produces="application/json")
    public ResponseEntity<String> addProductoConNombre(@PathVariable("nom") String nombre, @PathVariable("cat") String categoria) {
        System.out.println("Voy a añadir el producto con nombre " + nombre + " y categoría " + categoria);
        listaProductos.add(new ProductoModel(99, "NUEVO", 100.5,"negro"));
        return new ResponseEntity<>("Producto creado correctamente", HttpStatus.CREATED);
    }

    @PutMapping("/productos/{id}")
    public ResponseEntity<String> updateProducto(@PathVariable int id, @RequestBody ProductoModel cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            ProductoModel productoAModificar = listaProductos.get(id);
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(id, productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }

    @PutMapping("/productos")
    public ResponseEntity<String> updateProducto(@RequestBody ProductoModel cambios)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            ProductoModel productoAModificar = listaProductos.get(cambios.getId());
            System.out.println("Voy a modificar el producto");
            System.out.println("Precio actual: " + String.valueOf(productoAModificar.getPrecio()));
            System.out.println("Precio nuevo: " + String.valueOf(cambios.getPrecio()));
            productoAModificar.setNombre(cambios.getNombre());
            productoAModificar.setPrecio(cambios.getPrecio());
            listaProductos.set(cambios.getId(), productoAModificar);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }

    @PutMapping("/subirprecio")
    public ResponseEntity<String> subirPrecio()
    {
        ResponseEntity<String> resultado = null;
        for (ProductoModel p:listaProductos) {
            p.setPrecio(p.getPrecio()*1.25);
        }
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;

    }

    @DeleteMapping("/productos/{id}")
    public ResponseEntity<String> deleteProducto(@PathVariable int id)
    {
        ResponseEntity<String> resultado = null;
        try
        {
            ProductoModel productoAEliminar = listaProductos.get(id-1);
            listaProductos.remove(id-1);
            resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        }
        catch (Exception ex)
        {
            resultado = new ResponseEntity<>("No se ha encontrado el producto", HttpStatus.NOT_FOUND);
        }
        return resultado;

    }

    @DeleteMapping("/productos")
    public ResponseEntity<String> deleteProducto()
    {
        ResponseEntity<String> resultado = null;
        //listaProductos.removeAll(listaProductos);
        listaProductos.clear();
        //for (Producto p: listaProductos) {
        //    listaProductos.remove(p);
        //}
        resultado = new ResponseEntity<>(HttpStatus.NO_CONTENT);
        return resultado;

    }
}
