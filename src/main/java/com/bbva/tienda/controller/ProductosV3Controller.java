package com.bbva.tienda.controller;

import com.bbva.tienda.data.ProductoMongo;
import com.bbva.tienda.data.ProductoRepository;
import com.bbva.tienda.model.ProductoModel;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/v3")
public class ProductosV3Controller {

    @Autowired
    private ProductoRepository repository;

    /* Get lista de productos */
    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<ProductoMongo>> obtenerListado()
    {
        List<ProductoMongo> lista = repository.findAll();
        return new ResponseEntity<>(lista, HttpStatus.OK);
    }

    @PostMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<String> addProducto(@RequestBody ProductoModel productoModel)
    {
        ProductoMongo productoMongo = productoModel.map();
        ProductoMongo result = repository.insert(productoMongo);

        return new ResponseEntity<>(result.toString(),HttpStatus.OK);
    }




}
