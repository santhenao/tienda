package com.bbva.tienda.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class ProductoController {

    private List<String> listaProductos = null;

    public ProductoController() {
        listaProductos = new ArrayList<>();
        listaProductos.add("PR1");
        listaProductos.add("PR2");
    }

    @GetMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<List<String>> getProductos() {
        return new ResponseEntity<>(listaProductos, HttpStatus.OK);
    }

    @GetMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> getProducto(@PathVariable("id") int id) {

        ResponseEntity<String> response = null;
        try {

            response = new ResponseEntity<>(listaProductos.get(id), HttpStatus.OK);

        } catch (Exception exception) {

            response = new ResponseEntity<>("", HttpStatus.NOT_FOUND);
        }

        return response;
    }


    @PostMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<HttpStatus> postProducto() {

        listaProductos.add("nuevo");
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @PostMapping(value = "/productos/{nom}", produces = "application/json")
    public ResponseEntity<HttpStatus> postProductoConNombre(@PathVariable("nom") String nombre) {

        listaProductos.add(nombre);
        return new ResponseEntity<>(HttpStatus.CREATED);

    }

    @PutMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> putProducto(@PathVariable("id") int id, @RequestBody String producto){
        try {
            listaProductos.set(id,producto);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception exception){
            return new ResponseEntity<>("No se ha encontrado el producto",HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping(value = "/productos/{id}", produces = "application/json")
    public ResponseEntity<String> deleteProducto(@PathVariable("id") int id){
        try {

            listaProductos.remove(id);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);

        } catch (Exception exception) {
            return new ResponseEntity<>("No se ha encontrado el producto",HttpStatus.NOT_FOUND);
        }

    }


    /*DUDA DIFERENCIA PUT Y PATCH*/
    @PatchMapping(value = "/productos", produces = "application/json")
    public ResponseEntity<HttpStatus> patchProducto(){

        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }


}
