package com.bbva.tienda.data;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductoRepository extends MongoRepository<ProductoMongo, String> {

    public ProductoMongo findByNombre(String nombre);


}
