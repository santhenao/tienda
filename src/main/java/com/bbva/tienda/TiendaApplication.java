package com.bbva.tienda;

import com.bbva.tienda.data.ProductoMongo;
import com.bbva.tienda.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;

import java.util.Arrays;
import java.util.List;

@SpringBootApplication
public class TiendaApplication implements CommandLineRunner{


	@Autowired
	private ProductoRepository repository;

	public static void main(String[] args) {
		SpringApplication.run(TiendaApplication.class, args);
	}

	@Bean
	public CommandLineRunner commandLineRunner(ApplicationContext ctx) {
		return args -> {
			System.out.println("Investigación de Spring Boot");
			String[] beanNames = ctx.getBeanDefinitionNames();
			Arrays.sort(beanNames);
			for (String beanName : beanNames) {
				System.out.println(beanName);
			}
		};
	}

	@Override
	public void run(String... args) throws Exception {
		System.out.println("Preparando MongoDB");

		List<ProductoMongo> lista = repository.findAll();
		for (ProductoMongo p:lista) {
			System.out.println(p.toString());
		}
	}

}
