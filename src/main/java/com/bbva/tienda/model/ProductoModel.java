package com.bbva.tienda.model;


import com.bbva.tienda.data.ProductoMongo;

public class ProductoModel {

    private int id;
    private String nombre;
    private double precio;
    private String color;

    public ProductoModel(int id, String nombre, double precio, String color) {
        this.setId(id);
        this.setNombre(nombre);
        this.setPrecio(precio);
        this.setColor(color);
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public ProductoMongo map(){

        return new ProductoMongo(nombre,precio,color);

    }
}
